﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio1Parcial2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            label4.Text = trackBar1.Value.ToString();
            label5.Text = trackBar2.Value.ToString();

            if (trackBar1.Value >= (trackBar1.Maximum/2) && trackBar2.Value >= (trackBar1.Maximum / 2))
            {
                label1.Text = "Warning";
            }
            
            else label1.Text = "Ok";
        }
    }
}
